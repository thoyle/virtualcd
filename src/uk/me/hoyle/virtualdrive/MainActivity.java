package uk.me.hoyle.virtualdrive;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import com.ipaulpro.afilechooser.utils.FileUtils;

import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;

public class MainActivity extends Activity {
	
	private static final int REQUEST_ISO = 1;
	private static final int REQUEST_IMG = 2;
	private boolean mHasCdrom;

	DataOutputStream mShellOutput;
    DataInputStream mShellResult;
    Process mSuProcess;
   
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        if(!checkRoot()) {
        	new AlertDialog.Builder(this)
            .setIcon(R.drawable.ic_launcher)
            .setTitle(R.string.app_name)
            .setMessage(R.string.app_needs_root)
            .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                	finish();
                }
            })
            .create().show();
        }
        
        LinearLayout cd = (LinearLayout) findViewById(R.id.cd_image);
        LinearLayout hd = (LinearLayout) findViewById(R.id.hd_image);
        LinearLayout unmount = (LinearLayout) findViewById(R.id.unmount_image);
        
        cd.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if(!mHasCdrom) {
		        	new AlertDialog.Builder(MainActivity.this)
		            .setIcon(R.drawable.ic_launcher)
		            .setTitle(R.string.app_name)
		            .setMessage(R.string.app_needs_kernel)
		            .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
		                public void onClick(DialogInterface dialog, int whichButton) {
		                	finish();
		                }
		            })
		            .create().show();					
				}
				else
					createChooser(REQUEST_ISO);
			}       	
        });
	        
        mHasCdrom = true;
        File file = new File("/sys/class/android_usb/android0/f_mass_storage/lun/cdrom");
	    if(!file.exists()) {
	    	mHasCdrom = false;
        	cd.setAlpha((float)0.3);
        } 
        file = null;
        
        hd.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				createChooser(REQUEST_IMG);
			}       	
        });

        unmount.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				doUnmount();
			}       	
        });
        
        updateMounted();
    }
    
    /* This could be done as onPause/onResume but I'd rather not prompt the user
     * every time.
     */
    @Override
    public void onDestroy() {
    	super.onDestroy();
    	
    	if(mShellOutput != null) {
    		try {
    			mShellOutput.close();
    		} catch(IOException e)
    		{
    			e.printStackTrace();
    		}
    		mShellOutput = null;
    	}
    		
    	if(mShellResult != null) {
    		try {
    			mShellResult.close();
    		} catch(IOException e)
    		{
    			e.printStackTrace();
    		}
    		mShellResult = null;
    	}
    	
    	mSuProcess = null;
    }

/*    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
        default:
            return super.onOptionsItemSelected(item);
        }
    } */
    
    private boolean checkRoot() {

        boolean retval = false;
        
        try
        {
          mSuProcess = Runtime.getRuntime().exec("su");
          
          mShellOutput = 
              new DataOutputStream(mSuProcess.getOutputStream());
          mShellResult = 
              new DataInputStream(mSuProcess.getInputStream());
          
          if (null != mShellOutput && null != mShellResult)
          {
            // Getting the id of the current user to check if this is root
        	mShellOutput.writeBytes("id\n");
        	mShellOutput.flush();

            @SuppressWarnings("deprecation")
			String currUid = mShellResult.readLine();
            boolean exitSu = false;
            if (null == currUid)
            {
              retval = false;
              exitSu = false;
              Log.d("ROOT", "Can't get root access or denied by user");
            }
            else if (true == currUid.contains("uid=0"))
            {
              retval = true;
              exitSu = false;
              Log.d("ROOT", "Root access granted");
            }
            else
            {
              retval = false;
              exitSu = true;
              Log.d("ROOT", "Root access rejected: " + currUid);
            }

            if (exitSu)
            {
              mShellOutput.writeBytes("exit\n");
              mShellOutput.flush();
            }
          }
        }
        catch (Exception e)
        {
          // Can't get root !
          // Probably broken pipe exception on trying to write to output
          // stream after su failed, meaning that the device is not rooted
          
          retval = false;
          Log.d("ROOT", "Root access rejected [" +
                e.getClass().getName() + "] : " + e.getMessage());
        }

        return retval;
     }
    
	protected void doMount(File file, int requestCode) {
    	String old_classes, old_product, classes;
    	
    	if(!file.canRead()) {
			Toast.makeText(MainActivity.this, "Unable to read file", Toast.LENGTH_SHORT).show();
    	} else {
    		try {
    			File functions = new File("/sys/class/android_usb/android0/functions");
    			BufferedReader br = new BufferedReader(new FileReader(functions));
    			old_classes = br.readLine();
    			br.close();

    			File product = new File("/sys/class/android_usb/android0/idProduct");
    			br = new BufferedReader(new FileReader(product));
    			old_product = br.readLine();
    			br.close();
    			
    			if(!old_product.equals("cd00")) {
    				getPreferences(MODE_PRIVATE).edit().putString("stored_product", old_product).commit();    				
    			}
    			
    			if(!old_classes.contains("mass_storage")) {
    				getPreferences(MODE_PRIVATE).edit().putString("stored_functions", old_classes).commit();
    			
	    			StringBuilder b = new StringBuilder();
	    			for (String token : old_classes.split(",")) {
	    				if(token != "mass_storage" && token != "mtp" && token != "ptp") {
	    					if(b.length() > 0) b.append(',');
	    					b.append(token);
	    				}
	    			}
	    			if(b.length() > 0) b.append(',');
	    			b.append("mass_storage");
	    			classes = b.toString();
    			} else {
    				classes = old_classes;
    			}
    			
    			mShellOutput.writeChars("echo 0 >/sys/class/android_usb/android0/enable\n");
    			mShellOutput.writeChars("echo cd00 >/sys/class/android_usb/android0/idProduct\n");
				mShellOutput.writeChars("echo "+classes+" >/sys/class/android_usb/android0/functions\n");
				mShellOutput.writeChars("echo >/sys/class/android_usb/android0/f_mass_storage/lun/file\n");
				if(requestCode == REQUEST_ISO) {
					mShellOutput.writeChars("echo 1 >/sys/class/android_usb/android0/f_mass_storage/lun/ro\n");
					mShellOutput.writeChars("echo 1 >/sys/class/android_usb/android0/f_mass_storage/lun/cdrom\n");
				} else {
					mShellOutput.writeChars("echo 0 >/sys/class/android_usb/android0/f_mass_storage/lun/ro\n");
					mShellOutput.writeChars("echo 0 >/sys/class/android_usb/android0/f_mass_storage/lun/cdrom\n");					
				}
				mShellOutput.writeChars("echo 1 >/sys/class/android_usb/android0/enable\n");
	            mShellOutput.flush();
				mShellOutput.writeChars("echo '"+file.getAbsolutePath()+"' >/sys/class/android_usb/android0/f_mass_storage/lun/file\n");
	            mShellOutput.flush();
	            while(mShellResult.available() > 0) { mShellResult.read(); }

	            updateMounted();

    		} catch (IOException e) {
				e.printStackTrace();
			}   		
    	}	
    }
	
	protected void doUnmount() {
		try {
			mShellOutput.writeChars("echo 0 >/sys/class/android_usb/android0/enable\n");
			
			if(getPreferences(MODE_PRIVATE).contains("stored_functions")) {
				String classes = getPreferences(MODE_PRIVATE).getString("stored_functions", "");
				mShellOutput.writeChars("echo "+classes+" >/sys/class/android_usb/android0/functions\n");
			}
			
			if(getPreferences(MODE_PRIVATE).contains("stored_product")) {
				String product = getPreferences(MODE_PRIVATE).getString("stored_product", "");
				mShellOutput.writeChars("echo "+product+" >/sys/class/android_usb/android0/idProduct\n");
			}

			mShellOutput.writeChars("echo >/sys/class/android_usb/android0/f_mass_storage/lun/file\n");
			mShellOutput.writeChars("echo 1 >/sys/class/android_usb/android0/enable\n");
            mShellOutput.flush();
            while(mShellResult.available() > 0) { mShellResult.read(); }
            
            updateMounted();
		} catch (IOException e) {
			e.printStackTrace();
		}   		
	}
    
    protected void createChooser(int requestCode) {
    	int title;
    	
    	if(requestCode == REQUEST_ISO) title = R.string.iso_request_title;
    	else if(requestCode == REQUEST_IMG) title = R.string.img_request_title;
    	else title=R.string.app_name;
    	
        Intent target = FileUtils.createGetContentIntent();
        Intent intent = Intent.createChooser(target, getText(title));
        try {
            startActivityForResult(intent, requestCode);
        } catch (ActivityNotFoundException e) {
            // The reason for the existence of aFileChooser
        }  	
    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
        case REQUEST_ISO:
        case REQUEST_IMG:
            if (resultCode == RESULT_OK) {  
                // The URI of the selected file 
                final Uri uri = data.getData();
                // Create a File from this Uri
                File file = FileUtils.getFile(uri);
                doMount(file, requestCode);                
            }
        }
    }
    
    @SuppressWarnings("deprecation")
	protected void updateMounted() {
    	String line = "(unknown)";
    	
		try {
			mShellOutput.writeChars("cat /sys/class/android_usb/android0/f_mass_storage/lun/file\n");
			mShellOutput.flush();
			Thread.sleep(100);
			if(mShellResult.available() > 0)
				line = mShellResult.readLine();
			else
				line = "";			
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
        TextView mounted = (TextView)findViewById(R.id.currently_mounted);
        mounted.setText(line);
    }
}    

